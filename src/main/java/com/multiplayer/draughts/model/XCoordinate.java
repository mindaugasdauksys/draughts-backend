package com.multiplayer.draughts.model;

public enum XCoordinate {
    A(1), B(2), C(3), D(4), E(5), F(6), G(7), H(8);

    private final int num;

    XCoordinate(int num) {
        this.num = num;
    }

    public int getNum() {
        return num;
    }

    @Override
    public String toString() {
        return name().toLowerCase();
    }

    public static XCoordinate fromFullCoordinates(String coordinates) {
        return valueOf(String.valueOf(coordinates.charAt(0)).toUpperCase());
    }

    public static XCoordinate fromNumber(int number) {
        if (number < 1 || number > 8) {
            return null;
        }
        return values()[number - 1];
    }

    public XCoordinate add(int diff) {
        return fromNumber(getNum() + diff);
    }

}
