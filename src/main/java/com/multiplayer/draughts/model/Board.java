package com.multiplayer.draughts.model;

import com.multiplayer.draughts.utils.CoordinateUtils;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import static com.multiplayer.draughts.utils.CoordinateUtils.isValidCoordinate;
import static com.multiplayer.draughts.utils.CrowningUtils.isCrownCellForColor;

public class Board {
    private final Map<String, Draught> draughtsByCoordinates;
    private boolean isWhiteMove;

    public Board() {
        draughtsByCoordinates = new HashMap<>();
        initialize();
        isWhiteMove = true;
    }

    public Board(String[] boardRows) {
        draughtsByCoordinates = new HashMap<>();
        for (int xNum = 1; xNum <= 8; xNum++) {
            for (int yNum = 8; yNum >= 1; yNum--) {
                String draughtSymbol = String.valueOf(boardRows[yNum - 1].charAt(xNum - 1));
                switch (draughtSymbol){
                    case "W":
                        draughtsByCoordinates.put(XCoordinate.fromNumber(xNum).toString() + YCoordinate.fromNumber(8 - yNum + 1), new Draught(Color.WHITE, true));
                        break;
                    case "B":
                        draughtsByCoordinates.put(XCoordinate.fromNumber(xNum).toString() + YCoordinate.fromNumber(8 - yNum + 1), new Draught(Color.BLACK, true));
                        break;
                    case "w":
                        draughtsByCoordinates.put(XCoordinate.fromNumber(xNum).toString() + YCoordinate.fromNumber(8 - yNum + 1), new Draught(Color.WHITE));
                        break;
                    case "b":
                        draughtsByCoordinates.put(XCoordinate.fromNumber(xNum).toString() + YCoordinate.fromNumber(8 - yNum + 1), new Draught(Color.BLACK));
                        break;
                    default:
                        draughtsByCoordinates.put(XCoordinate.fromNumber(xNum).toString() + YCoordinate.fromNumber(8 - yNum + 1), null);
                }
            }
        }
        isWhiteMove = true;
    }

    public Board(Board board){
        this.draughtsByCoordinates = new HashMap<>(board.draughtsByCoordinates);
        this.isWhiteMove = board.isWhiteMove;
    }

    private void initialize() {
        for (YCoordinate y : YCoordinate.values()) {
            for (XCoordinate x : XCoordinate.values()) {
                if (isValidCoordinate(x, y) && y.getNum() <= 3) {
                    draughtsByCoordinates.put(x.toString() + y, new Draught(Color.WHITE));
                } else if (isValidCoordinate(x, y) && y.getNum() >= 6) {
                    draughtsByCoordinates.put(x.toString() + y, new Draught(Color.BLACK));
                } else {
                    draughtsByCoordinates.put(x.toString() + y, null);
                }
            }
        }
    }

    public Draught getDraughtByCoordinate(String coordinate) {
        return draughtsByCoordinates.get(coordinate);
    }

    public boolean isFreeCellToMove(String coordinate) {
        return coordinate != null && draughtsByCoordinates.get(coordinate) == null;
    }

    public List<String> coordinatesByColorAndKing(Color color, boolean isKing) {
        List<String> coordinates = new ArrayList<>();
        for (Map.Entry<String, Draught> draughtByCoordinate : draughtsByCoordinates.entrySet()) {
            Draught draught = draughtByCoordinate.getValue();
            if (draught != null && draught.isKing() == isKing && draught.getColor() == color) {
                coordinates.add(draughtByCoordinate.getKey());
            }
        }
        return coordinates;
    }

    public void move(MoveNode move) {
        Draught draught = draughtsByCoordinates.get(move.getCurrentCell());
        boolean becameKing = !move.isMoreMoves() && isCrownCellForColor(move.getEndCell(), draught.getColor());
        clearPath(move.getCurrentCell(), move.getEndCell());
        draughtsByCoordinates.put(move.getEndCell(), new Draught(draught.getColor(), becameKing || draught.isKing()));
    }

    private void clearPath(String startCoordinate, String endCoordinate) {
        Direction direction = CoordinateUtils.determineDirection(startCoordinate, endCoordinate);
        String coordinate = startCoordinate;
        clearCell(coordinate);
        do {
            coordinate = CoordinateUtils.incrementCoordinateByDirection(coordinate, direction);
            clearCell(coordinate);
        } while (!coordinate.equals(endCoordinate));
    }

    public void clearCell(String coordinate) {
        setDraughtAtCell(coordinate, null);
    }

    public void setDraughtAtCell(String coordinate, Draught draught) {
        draughtsByCoordinates.put(coordinate, draught);
    }

    public void print() {
        for(int y = 8; y >= 1; y--) {
            for(int x = 1; x <= 8; x++){
                XCoordinate xCoordinate = XCoordinate.fromNumber(x);
                YCoordinate yCoordinate = YCoordinate.fromNumber(y);
                if(!CoordinateUtils.isValidCoordinate(xCoordinate, yCoordinate)) {
                    System.out.print(" ");
                    continue;
                }

                Draught draught = draughtsByCoordinates.get("" + xCoordinate + yCoordinate);
                if (draught == null) {
                    System.out.print("_");
                    continue;
                }
                if (draught.isKing()) {
                    if (draught.getColor() == Color.WHITE) {
                        System.out.print("W");
                    } else {
                        System.out.print("B");
                    }
                } else {
                    if (draught.getColor() == Color.WHITE) {
                        System.out.print("w");
                    } else {
                        System.out.print("b");
                    }
                }
            }
            System.out.println();
        }
    }

}
