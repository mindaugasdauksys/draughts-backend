package com.multiplayer.draughts.model.movefinders;

import com.multiplayer.draughts.model.Board;
import com.multiplayer.draughts.model.Color;
import com.multiplayer.draughts.model.MoveNode;

import java.util.ArrayList;
import java.util.List;

public abstract class AbstractMovesFinder {

    private final Board board;
    private final Color color;

    public AbstractMovesFinder(Board board, Color color) {
        this.board = board;
        this.color = color;
    }

    public List<MoveNode> find() {
        List<MoveNode> availableMoves = new ArrayList<>();
        for(String draughtCoordinate : getDraughtsToCheck()) {
            availableMoves.addAll(findPieceMoves(draughtCoordinate));
        }
        return availableMoves;
    }

    private List<String> getDraughtsToCheck() {
        return board.coordinatesByColorAndKing(color, isKingCheck());
    }

    public Board getBoard() {
        return board;
    }

    public Color getColor() {
        return color;
    }

    protected abstract List<MoveNode> findPieceMoves(String currentCoordinate);
    protected abstract boolean isKingCheck();
}
