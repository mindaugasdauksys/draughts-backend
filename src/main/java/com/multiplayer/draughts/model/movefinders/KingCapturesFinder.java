package com.multiplayer.draughts.model.movefinders;

import com.multiplayer.draughts.model.Color;
import com.multiplayer.draughts.model.Direction;
import com.multiplayer.draughts.model.Board;
import com.multiplayer.draughts.utils.CoordinateUtils;

import java.util.ArrayList;
import java.util.List;

public class KingCapturesFinder extends AbstractCapturesFinder {
    public KingCapturesFinder(Board board, Color color) {
        super(board, color);
    }

    @Override
    protected boolean isKingCheck() {
        return true;
    }

    @Override
    protected List<String> findAvailableMovesAfterCoordinate(String coordinate, Direction direction, Board board) {
        List<String> result = new ArrayList<>();
        while (coordinate != null) {
            coordinate = CoordinateUtils.incrementCoordinateByDirection(coordinate, direction);
            if (!board.isFreeCellToMove(coordinate)) {
                break;
            }
            result.add(coordinate);
        }

        return result;
    }

    @Override
    protected String findObstacle(Board board, String kingCoordinate, Direction direction) {
        while (true) {
            kingCoordinate = CoordinateUtils.incrementCoordinateByDirection(kingCoordinate, direction);
            if (kingCoordinate == null) {
                return null;
            }
            if (board.getDraughtByCoordinate(kingCoordinate) != null) {
                return kingCoordinate;
            }
        }
    }
}
