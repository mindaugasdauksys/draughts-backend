package com.multiplayer.draughts.model.movefinders;

import com.multiplayer.draughts.model.Board;
import com.multiplayer.draughts.model.CaptureNode;
import com.multiplayer.draughts.model.Color;
import com.multiplayer.draughts.model.MoveNode;
import com.multiplayer.draughts.utils.CaptureUtils;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.Stream;

public class AvailableMovesFinder {

    private final DraughtMovesFinder draughtMovesFinder;
    private final KingMovesFinder kingMovesFinder;
    private final KingCapturesFinder kingCapturesFinder;
    private final DraughtCapturesFinder draughtCapturesFinder;

    public AvailableMovesFinder(Board board, Color color) {
        this.draughtMovesFinder = new DraughtMovesFinder(board, color);
        this.kingMovesFinder = new KingMovesFinder(board, color);
        this.kingCapturesFinder = new KingCapturesFinder(board, color);
        this.draughtCapturesFinder = new DraughtCapturesFinder(board, color);
    }

    public List<MoveNode> find() {
        List<CaptureNode> captureNodes = findAvailableCapturesForColor();
        if (!captureNodes.isEmpty()) {
            return captureNodes.stream().map(MoveNode::fromCapture).distinct().collect(Collectors.toList());
        }

        return findAvailableMovesForColor();
    }

    private List<MoveNode> findAvailableMovesForColor() {
        List<MoveNode> draughtsMoves = draughtMovesFinder.find();
        List<MoveNode> kingsMoves = kingMovesFinder.find();
        return Stream.concat(draughtsMoves.stream(), kingsMoves.stream()).collect(Collectors.toList());
    }

    private List<CaptureNode> findAvailableCapturesForColor() {
        List<CaptureNode> kingsCaptures = kingCapturesFinder.find();
        List<CaptureNode> draughtsCaptures = draughtCapturesFinder.find();
        List<CaptureNode> allCaptures = Stream.concat(kingsCaptures.stream(), draughtsCaptures.stream()).collect(Collectors.toList());

        int maxCapturesDepth = 0;
        for (CaptureNode captures : allCaptures) {
            int capturesDepth = CaptureUtils.findMaxCapturesCount(captures);
            if (maxCapturesDepth < capturesDepth) {
                maxCapturesDepth = capturesDepth;
            }
        }

        List<CaptureNode> availableCaptures = new ArrayList<>();
        for (CaptureNode captureNode : allCaptures) {
            CaptureUtils.findDeepestPaths(captureNode, 0, maxCapturesDepth, availableCaptures);
        }
        return availableCaptures;
    }
}
