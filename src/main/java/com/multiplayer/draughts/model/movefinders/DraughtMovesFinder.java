package com.multiplayer.draughts.model.movefinders;

import com.multiplayer.draughts.model.Board;
import com.multiplayer.draughts.model.Color;
import com.multiplayer.draughts.model.Direction;
import com.multiplayer.draughts.model.MoveNode;
import com.multiplayer.draughts.utils.CoordinateUtils;

import java.util.ArrayList;
import java.util.List;

public final class DraughtMovesFinder extends AbstractMovesFinder {

    public DraughtMovesFinder(Board board, Color color) {
        super(board, color);
    }

    @Override
    protected List<MoveNode> findPieceMoves(String draughtCoordinate) {
        if (getColor() == Color.WHITE) {
            return findAvailableMovesByDirections(draughtCoordinate, Direction.TOP_LEFT, Direction.TOP_RIGHT);
        }
        return findAvailableMovesByDirections(draughtCoordinate, Direction.BOTTOM_LEFT, Direction.BOTTOM_RIGHT);
    }

    @Override
    protected boolean isKingCheck() {
        return false;
    }

    private List<MoveNode> findAvailableMovesByDirections(String draughtCoordinate, Direction... directions) {
        List<MoveNode> result = new ArrayList<>();
        for (Direction direction : directions) {
            String move = CoordinateUtils.incrementCoordinateByDirection(draughtCoordinate, direction);
            if (move != null && getBoard().isFreeCellToMove(move)) {
                result.add(new MoveNode(draughtCoordinate, move, false));
            }
        }
        return result;
    }
}
