package com.multiplayer.draughts.model.movefinders;

import com.multiplayer.draughts.model.Board;
import com.multiplayer.draughts.model.Color;
import com.multiplayer.draughts.model.Direction;
import com.multiplayer.draughts.utils.CoordinateUtils;

import java.util.ArrayList;
import java.util.List;

public class DraughtCapturesFinder extends AbstractCapturesFinder {

    public DraughtCapturesFinder(Board board, Color color) {
        super(board, color);
    }

    @Override
    protected boolean isKingCheck() {
        return false;
    }

    @Override
    protected String findObstacle(Board board, String draughtCoordinate, Direction direction) {
        draughtCoordinate = CoordinateUtils.incrementCoordinateByDirection(draughtCoordinate, direction);
        if (draughtCoordinate == null || board.getDraughtByCoordinate(draughtCoordinate) == null) {
            return null;
        }
        return draughtCoordinate;
    }

    @Override
    protected List<String> findAvailableMovesAfterCoordinate(String coordinate, Direction direction, Board board) {
        List<String> result = new ArrayList<>();
        if (coordinate != null) {
            coordinate = CoordinateUtils.incrementCoordinateByDirection(coordinate, direction);
            if (board.isFreeCellToMove(coordinate)) {
                result.add(coordinate);
            }
        }

        return result;
    }

}
