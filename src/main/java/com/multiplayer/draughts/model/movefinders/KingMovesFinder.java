package com.multiplayer.draughts.model.movefinders;

import com.multiplayer.draughts.model.Board;
import com.multiplayer.draughts.model.Color;
import com.multiplayer.draughts.model.Direction;
import com.multiplayer.draughts.model.MoveNode;
import com.multiplayer.draughts.utils.CoordinateUtils;

import java.util.ArrayList;
import java.util.List;

public final class KingMovesFinder extends AbstractMovesFinder {

    public KingMovesFinder(Board board, Color color) {
        super(board, color);
    }

    @Override
    protected List<MoveNode> findPieceMoves(String kingCoordinate) {
        List<MoveNode> kingMoves = new ArrayList<>();
        for (Direction direction : Direction.values()) {
            kingMoves.addAll(findMovesByDirection(kingCoordinate, direction));
        }
        return kingMoves;
    }

    @Override
    protected boolean isKingCheck() {
        return true;
    }

    private List<MoveNode> findMovesByDirection(String kingCoordinate, Direction direction) {
        List<MoveNode> result = new ArrayList<>();
        String moveToAdd = CoordinateUtils.incrementCoordinateByDirection(kingCoordinate, direction);
        while (moveToAdd != null && getBoard().isFreeCellToMove(moveToAdd)) {
            result.add(new MoveNode(kingCoordinate, moveToAdd, false));
            moveToAdd = CoordinateUtils.incrementCoordinateByDirection(moveToAdd, direction);
        }
        return result;
    }
}
