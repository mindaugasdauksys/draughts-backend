package com.multiplayer.draughts.model.movefinders;

import com.multiplayer.draughts.model.Board;
import com.multiplayer.draughts.model.CaptureNode;
import com.multiplayer.draughts.model.Color;
import com.multiplayer.draughts.model.Direction;
import com.multiplayer.draughts.model.Draught;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

public abstract class AbstractCapturesFinder {

    private final Board board;
    private final Color color;

    public AbstractCapturesFinder(Board board, Color color) {
        this.board = board;
        this.color = color;
    }

    public List<CaptureNode> find() {
        List<CaptureNode> captureNodes = new ArrayList<>();
        for(String draughtCoordinate : getDraughtsToCheck()) {
            CaptureNode captureNode = new CaptureNode(draughtCoordinate, null);
            findCaptures(board, captureNode, null);
            if (!captureNode.isLeaf()) {
                captureNodes.add(captureNode);
            }
        }
        return captureNodes;
    }

    private List<String> getDraughtsToCheck() {
        return board.coordinatesByColorAndKing(color, isKingCheck());
    }

    private void findCaptures(Board board, CaptureNode captureNode, Direction fromDirection) {
        for(Direction direction : Direction.values()) {
            if (direction == Direction.oppositeTo(fromDirection)) {
                continue;
            }

            String obstacleCoordinate = findObstacle(board, captureNode.getCurrentCell(), direction);
            if (obstacleCoordinate == null) {
                continue;
            }

            Draught obstacle = board.getDraughtByCoordinate(obstacleCoordinate);
            if (obstacle.getColor() == color) {
                continue;
            }

            List<String> availableMoves = findAvailableMovesAfterCoordinate(obstacleCoordinate, direction, board);
            if (availableMoves.isEmpty()) {
                continue;
            }

            List<CaptureNode> availableCaptures = availableMoves.stream()
                .map(move -> new CaptureNode(move, captureNode)).collect(Collectors.toList());

            captureNode.addCaptures(availableCaptures);

            for (CaptureNode newCapture : availableCaptures) {
                Board imaginaryBoard = new Board(board);
                imaginaryBoard.clearCell(obstacleCoordinate);
                imaginaryBoard.clearCell(captureNode.getCurrentCell());
                imaginaryBoard.setDraughtAtCell(newCapture.getCurrentCell(), new Draught(color, isKingCheck()));
                findCaptures(imaginaryBoard, newCapture, direction);
            }
        }
    }

    protected abstract boolean isKingCheck();
    protected abstract String findObstacle(Board board, String currentCoordinate, Direction direction);
    protected abstract List<String> findAvailableMovesAfterCoordinate(String coordinate, Direction direction, Board board);
}
