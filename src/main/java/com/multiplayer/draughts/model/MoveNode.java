package com.multiplayer.draughts.model;

public class MoveNode {

    private final String currentCell;
    private final String endCell;
    private boolean moreMoves;

    public MoveNode(String currentCell, String endCell, boolean moreMoves) {
        this.currentCell = currentCell;
        this.endCell = endCell;
        this.moreMoves = moreMoves;
    }

    public String getCurrentCell() {
        return currentCell;
    }

    public String getEndCell() {
        return endCell;
    }

    public boolean isMoreMoves() {
        return moreMoves;
    }

    public static MoveNode fromCapture(CaptureNode captureNode) {
        return new MoveNode(captureNode.getCurrentCell(), captureNode.getCaptureCell(), !captureNode.getCaptures().get(0).isLeaf());
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        MoveNode moveNode = (MoveNode) o;

        if (moreMoves != moveNode.moreMoves) return false;
        if (!currentCell.equals(moveNode.currentCell)) return false;
        return endCell.equals(moveNode.endCell);
    }

    @Override
    public int hashCode() {
        int result = currentCell.hashCode();
        result = 31 * result + endCell.hashCode();
        result = 31 * result + (moreMoves ? 1 : 0);
        return result;
    }

    @Override
    public String toString() {
        return "MoveNode(" + currentCell + ", " + endCell + ", " + moreMoves + ')';
    }
}
