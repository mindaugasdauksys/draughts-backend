package com.multiplayer.draughts.model;

public enum Color {
    WHITE, BLACK
}
