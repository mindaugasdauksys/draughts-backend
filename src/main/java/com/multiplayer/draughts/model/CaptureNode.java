package com.multiplayer.draughts.model;

import java.util.ArrayList;
import java.util.List;

public class CaptureNode {
    private CaptureNode cameFrom;
    private final String currentCell;
    private final List<CaptureNode> captures;

    public CaptureNode(String currentCell, CaptureNode cameFrom) {
        this.currentCell = currentCell;
        this.cameFrom = cameFrom;
        this.captures = new ArrayList<>();
    }

    public CaptureNode(String currentCell) {
        this.currentCell = currentCell;
        this.captures = new ArrayList<>();
    }

    public void addCaptures(List<CaptureNode> captures) {
        this.captures.addAll(captures);
    }

    public void addCapture(CaptureNode capture) {
        this.captures.add(capture);
    }

    public String getCurrentCell() {
        return currentCell;
    }

    public List<CaptureNode> getCaptures() {
        return captures;
    }

    public String getCaptureCell() {
        assert this.captures.size() == 1;
        return this.captures.get(0).currentCell;
    }

    public boolean isLeaf() {
        return captures.isEmpty();
    }

    public CaptureNode getCameFrom() {
        return cameFrom;
    }

    public void print() {
        int i = 0;
        do {
            System.out.print(currentCell);
            if (i < captures.size()) {
                System.out.print(" -> ");
                captures.get(i).print();
            } else {
                System.out.println();
            }
            i++;
        }  while (i < captures.size());
    }

}
