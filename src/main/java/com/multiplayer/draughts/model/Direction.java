package com.multiplayer.draughts.model;

public enum Direction {
    BOTTOM_LEFT(-1, -1),
    BOTTOM_RIGHT(+1, -1),
    TOP_LEFT(-1, +1),
    TOP_RIGHT(+1, +1);

    private final int xDiff;
    private final int yDiff;
    Direction(int xDiff, int yDiff) {
        this.xDiff = xDiff;
        this.yDiff = yDiff;
    }

    public static Direction byDiff(int xDiff, int yDiff) {
        for(Direction direction : values()) {
            if (direction.xDiff == xDiff && direction.yDiff == yDiff) {
                return direction;
            }
        }

        throw new IllegalArgumentException("Cannot find direction by xDiff: " + xDiff + " yDiff: " + yDiff);
    }

    public static Direction oppositeTo(Direction direction) {
        if (direction == null) {
            return null;
        }
        switch (direction) {
            case TOP_LEFT:
                return BOTTOM_RIGHT;
            case TOP_RIGHT:
                return BOTTOM_LEFT;
            case BOTTOM_LEFT:
                return TOP_RIGHT;
            case BOTTOM_RIGHT:
                return TOP_LEFT;
            default:
                return null;
        }
    }

    public int getxDiff() {
        return xDiff;
    }

    public int getyDiff() {
        return yDiff;
    }
}
