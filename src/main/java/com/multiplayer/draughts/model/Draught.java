package com.multiplayer.draughts.model;

public class Draught {

    private final Color color;
    private boolean isKing;

    public Draught(Color color) {
        this.color = color;
    }

    public Draught(Color color, boolean isKing) {
        this.color = color;
        this.isKing = isKing;
    }

    public Color getColor() {
        return color;
    }

    public boolean isKing() {
        return isKing;
    }

    public void setKing(boolean king) {
        isKing = king;
    }
}
