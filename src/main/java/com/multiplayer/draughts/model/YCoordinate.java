package com.multiplayer.draughts.model;

public enum YCoordinate {
    N1(1), N2(2), N3(3), N4(4), N5(5), N6(6), N7(7), N8(8);

    private final int num;

    YCoordinate(int num) {
        this.num = num;
    }

    public int getNum() {
        return num;
    }

    @Override
    public String toString() {
        return Integer.toString(num);
    }

    public static YCoordinate fromFullCoordinates(String coordinates) {
        return valueOf("N" + coordinates.charAt(1));
    }

    public static YCoordinate fromNumber(int number) {
        if (number < 1 || number > 8) {
            return null;
        }
        return values()[number - 1];
    }

    public YCoordinate add(int diff) {
        return fromNumber(getNum() + diff);
    }
}
