package com.multiplayer.draughts.utils;

import com.multiplayer.draughts.model.CaptureNode;

import java.util.List;

public class CaptureUtils {

    public static int findMaxCapturesCount(CaptureNode captureNode) {
        if (captureNode.getCaptures().isEmpty()) {
            return 0;
        }
        int max = 0;
        for (CaptureNode child : captureNode.getCaptures()) {
            int captures = findMaxCapturesCount(child);
            if (captures > max) {
                max = captures;
            }
        }
        return 1 + max;
    }

    public static void findDeepestPaths(CaptureNode captures, int depth, int mustBeDepth, List<CaptureNode> results) {
        if (depth == mustBeDepth) {
            CaptureNode previousNode = new CaptureNode(captures.getCurrentCell());
            CaptureNode currentNode = null;
            while (captures.getCameFrom() != null) {
                captures = captures.getCameFrom();
                currentNode = new CaptureNode(captures.getCurrentCell());
                currentNode.addCapture(previousNode);
                previousNode = currentNode;
            }
            results.add(currentNode);
            return;
        }
        for(CaptureNode capture : captures.getCaptures()) {
            findDeepestPaths(capture, depth + 1, mustBeDepth, results);
        }
    }
}
