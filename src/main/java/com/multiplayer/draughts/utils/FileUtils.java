package com.multiplayer.draughts.utils;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

public class FileUtils {
    public static List<String> readLines(String filePath) {
        var lines = new ArrayList<String>();
        try {
            var reader = new BufferedReader(new FileReader(filePath));
            String line = reader.readLine();
            while (line != null) {
                lines.add(line);
                line = reader.readLine();
            }
        } catch (IOException e) {
            throw new IllegalArgumentException(e);
        }
        return lines;
    }

    public static List<String> readMoves(String filePath) {
        List<String> lines = readLines(filePath);
        var moves = new ArrayList<String>();
        for (String line : lines) {
            List<String> moveParts = List.of(line.split(" "));
            moves.addAll(moveParts.subList(1, moveParts.size()));
        }

        return moves;
    }

    private FileUtils() {}
}
