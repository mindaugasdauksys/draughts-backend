package com.multiplayer.draughts.utils;

import com.multiplayer.draughts.model.Color;

public class CrowningUtils {

    public static boolean isCrownCellForColor(String cell, Color color) {
        if (color == Color.WHITE) {
            return cell.matches("^.8$");
        }
        return cell.matches("^.1$");
    }
}
