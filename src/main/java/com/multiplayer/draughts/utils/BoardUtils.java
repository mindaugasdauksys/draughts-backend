package com.multiplayer.draughts.utils;

import com.multiplayer.draughts.model.Board;
import com.multiplayer.draughts.model.MoveNode;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.util.List;

public class BoardUtils {

    public static String[] readBoardFromFile(String filePath) {
        try {
            var result = new String[8];
            var reader = new BufferedReader(new FileReader(filePath));
            for (int i = 0; i < 8; i++) {
                result[i] = reader.readLine();
            }
            return result;
        } catch (IOException e) {
            throw new IllegalStateException(e);
        }
    }


    public static Board byMovesFromFile(String filePath) {
        List<String> moves = FileUtils.readMoves(filePath);
        List<MoveNode> moveNodes = MoveNodeParser.mapFromMoveList(moves);
        Board board = new Board();
        moveNodes.forEach(board::move);
        return board;
    }

    private BoardUtils() {}
}
