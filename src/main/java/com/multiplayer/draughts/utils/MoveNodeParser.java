package com.multiplayer.draughts.utils;

import com.multiplayer.draughts.model.MoveNode;

import java.util.ArrayList;
import java.util.List;

public class MoveNodeParser {

    public static List<MoveNode> mapFromMoveList(List<String> moves) {
        var moveNodes = new ArrayList<MoveNode>();
        for (String move : moves) {
            moveNodes.addAll(parseFromString(move));
        }
        return moveNodes;
    }

    private static List<MoveNode> parseFromString(String move) {
        if (move.contains("-")) {
            return convertToMove(move.split("-"));
        }
        return convertToCapture(move.split("x"));
    }

    private static List<MoveNode> convertToMove(String[] moveParts) {
        return List.of(new MoveNode(moveParts[0], moveParts[1], false));
    }

    private static List<MoveNode> convertToCapture(String[] moveParts) {
        var moveNodes = new ArrayList<MoveNode>();
        for (int i = 0; i < moveParts.length - 1; i++) {
            moveNodes.add(new MoveNode(moveParts[i], moveParts[i + 1], i < moveParts.length - 2));
        }
        return moveNodes;
    }

    private MoveNodeParser() {}
}
