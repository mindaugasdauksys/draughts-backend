package com.multiplayer.draughts.utils;

import com.multiplayer.draughts.model.Direction;
import com.multiplayer.draughts.model.XCoordinate;
import com.multiplayer.draughts.model.YCoordinate;

import static java.lang.Integer.signum;

public final class CoordinateUtils {

    public static boolean isValidCoordinate(XCoordinate x, YCoordinate y) {
        return (x.getNum() + y.getNum()) % 2 == 0;
    }

    public static String incrementCoordinateByDirection(String coordinate, Direction direction) {
        XCoordinate x0 = XCoordinate.fromFullCoordinates(coordinate);
        YCoordinate y0 = YCoordinate.fromFullCoordinates(coordinate);
        XCoordinate x1 = XCoordinate.fromNumber(x0.getNum() + direction.getxDiff());
        YCoordinate y1 = YCoordinate.fromNumber(y0.getNum() + direction.getyDiff());

        return formatString(x1, y1);
    }

    public static Direction determineDirection(String coordinate0, String coordinate1) {
        XCoordinate x0 = XCoordinate.fromFullCoordinates(coordinate0);
        YCoordinate y0 = YCoordinate.fromFullCoordinates(coordinate0);
        XCoordinate x1 = XCoordinate.fromFullCoordinates(coordinate1);
        YCoordinate y1 = YCoordinate.fromFullCoordinates(coordinate1);

        return Direction.byDiff(signum(x1.getNum() - x0.getNum()), signum(y1.getNum() - y0.getNum()));
    }

    public static String formatString(XCoordinate x, YCoordinate y) {
        if (x == null || y == null) {
            return null;
        }

        return x.toString() + y;
    }

    private CoordinateUtils() { }
}
