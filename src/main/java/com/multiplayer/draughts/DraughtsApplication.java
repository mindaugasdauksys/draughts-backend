package com.multiplayer.draughts;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class DraughtsApplication {

    public static void main(String[] args) {
        SpringApplication.run(DraughtsApplication.class, args);
    }

}

//    public Map<String, Draught> getDraughtsByCoordinates() {
//        return draughtsByCoordinates;
//    }

//    public boolean moveDraught(String startCoordinate, String endCoordinate) {
//        if (!isValidCoordinate(endCoordinate) || !isEmptyCoordinate(endCoordinate) ||
//            isEmptyCoordinate(startCoordinate) || !isDiagonalMove(startCoordinate, endCoordinate)) {
//            return false;
//        }
//
//
//    }
//
//    public boolean isValidCoordinate(String coordinate) {
//        return draughtsByCoordinates.containsKey(coordinate);
//    }
//
//    public boolean isEmptyCoordinate(String coordinate) {
//        return draughtsByCoordinates.get(coordinate) == null;
//    }
//
//    public boolean isValidMoveByDraught(String startCoordinate, String endCoordinate, Draught draught) {
//        return draught.isKing() ||
//            (moveLength(startCoordinate, endCoordinate) == 1 &&
//            (draught.getColor() == Color.WHITE && isMoveToTop(startCoordinate, endCoordinate)) ||
//            (draught.getColor() == Color.BLACK && !isMoveToTop(startCoordinate, endCoordinate))) ||
//            (moveLength(startCoordinate, endCoordinate) == 3 && )
//            ;
//    }
//
//    // MOVE TO UTILSx
//    public boolean isDiagonalMove(String startCoordinate, String endCoordinate) {
//        return isDiagonalMove(parseX(startCoordinate), parseY(startCoordinate), parseX(endCoordinate),
//            parseY(startCoordinate));
//    }
//
//    public boolean isDiagonalMove(int x0, String y0, int x1, String y1) {
//        return moveLength(x1, x0) == moveLength(numberAtX(y1), numberAtX(y0));
//    }
//
//    private int parseY(String coordinate) {
//        return Integer.parseInt(String.valueOf(coordinate.charAt(0)));
//    }
//
//    private String parseX(String coordinate) {
//        return String.valueOf(coordinate.charAt(1));
//    }
//
//    private int numberAtX(String y) {
//        return Arrays.binarySearch(X_COORDINATES, y) + 1;
//    }
//
//    public int moveLength(int a0, int a1) {
//        return Math.abs(a1 - a0);
//    }
//
//    public int moveLength(String startCoordinate, String endCoordinate) {
//        return moveLength(parseX(startCoordinate), parseX(endCoordinate));
//    }
//
//    boolean isMoveToTop(String startCoordinate, String endCoordinate) {
//        return numberAtX(parseY(endCoordinate) > numberAtX(parseY(startCoordinate);
//    }