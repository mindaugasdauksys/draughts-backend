package com.multiplayer.draughts.web;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.messaging.handler.annotation.DestinationVariable;
import org.springframework.messaging.handler.annotation.MessageMapping;
import org.springframework.messaging.simp.SimpMessagingTemplate;
import org.springframework.messaging.simp.annotation.SubscribeMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class RoomController {

    private final SimpMessagingTemplate template;

    @Autowired
    public RoomController(SimpMessagingTemplate template) {
        this.template = template;
    }

    @SubscribeMapping("/room/{roomId}")
    public Object enter() {
        return null;
    }

    @MessageMapping("/room/{roomId}/takePosition")
    public Object takePosition(@DestinationVariable String roomId, String position) {
        return null;
    }

    @MessageMapping("/room/{roomId}/start")
    public Object startGame(@DestinationVariable String roomId) {
        return null;
    }

    @MessageMapping("/room/{roomId}/move")
    public TableResponse movePiece(@DestinationVariable String roomId, TurnRequest turnRequest) {
        return null;
    }

    @MessageMapping("/room/{roomId}/drawReject")
    public Object rejectDraw() {
        return null;
    }

    @MessageMapping("/room/{roomId}/drawAccept")
    public Object acceptDraw() {
        return null;
    }

    @MessageMapping("/room/{roomId}/drawRequest")
    public Object requestDraw(){
        return null;
    }

    @MessageMapping("/room/{roomId}/undoReject")
    public Object rejectUndo() {
        return null;
    }

    @MessageMapping("/room/{roomId}/undoAccept")
    public Object acceptUndo() {
        return null;
    }

    @MessageMapping("/room/{roomId}/undoRequest")
    public Object requestUndo(){
        return null;
    }

    @MessageMapping("/room/{roomId}/surrender")
    public Object surrender() {
        return null;
    }

    @MessageMapping("/room/{roomId}/leave")
    public Object leave() {
        return null;
    }

}
