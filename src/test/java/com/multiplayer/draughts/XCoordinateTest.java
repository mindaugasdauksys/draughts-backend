package com.multiplayer.draughts;

import com.multiplayer.draughts.model.XCoordinate;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;

public class XCoordinateTest {
    @Test
    public void testsParsingFromFullCoordinates() {
        assertEquals(XCoordinate.fromFullCoordinates("a5"), XCoordinate.A);
    }

    @Test
    public void testsToString() {
        assertEquals(XCoordinate.A.toString(), "a");
    }
}
