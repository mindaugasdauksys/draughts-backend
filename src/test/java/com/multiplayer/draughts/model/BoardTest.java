package com.multiplayer.draughts.model;

import org.junit.jupiter.api.Test;


import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNull;

public class BoardTest {
    @Test
    public void correctlyInitializesDraughts() {
        Board board = new Board();

        assertEquals(board.getDraughtByCoordinate("a1").getColor(), Color.WHITE);
        assertEquals(board.getDraughtByCoordinate("c1").getColor(), Color.WHITE);
        assertEquals(board.getDraughtByCoordinate("h6").getColor(), Color.BLACK);
        assertNull(board.getDraughtByCoordinate("a2"));
        assertNull(board.getDraughtByCoordinate("h7"));
        assertNull(board.getDraughtByCoordinate("z7"));
    }
}
