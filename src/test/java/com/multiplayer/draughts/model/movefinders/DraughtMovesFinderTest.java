package com.multiplayer.draughts.model.movefinders;

import com.multiplayer.draughts.model.Board;
import com.multiplayer.draughts.model.Color;
import com.multiplayer.draughts.model.MoveNode;
import com.multiplayer.draughts.model.movefinders.DraughtMovesFinder;
import com.multiplayer.draughts.utils.BoardUtils;
import org.junit.jupiter.api.Test;

import java.util.List;

import static com.multiplayer.draughts.utils.TestFileUtils.getFullPath;
import static org.junit.jupiter.api.Assertions.assertEquals;

public class DraughtMovesFinderTest {

    @Test
    public void testsWhiteMoves() {
        Board board = new Board(BoardUtils.readBoardFromFile(getFullPath("test_table_04.txt")));
        List<MoveNode> whiteMoves = new DraughtMovesFinder(board, Color.WHITE).find();
        assertEquals(3, whiteMoves.size());
    }

    @Test
    public void testsBlackMoves() {
        Board board = new Board(BoardUtils.readBoardFromFile(getFullPath("test_table_04.txt")));
        List<MoveNode> blackMoves = new DraughtMovesFinder(board, Color.BLACK).find();
        assertEquals(3, blackMoves.size());
    }
}
