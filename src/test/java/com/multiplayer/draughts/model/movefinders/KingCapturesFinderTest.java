package com.multiplayer.draughts.model.movefinders;

import com.multiplayer.draughts.model.Board;
import com.multiplayer.draughts.model.CaptureNode;
import com.multiplayer.draughts.model.Color;
import com.multiplayer.draughts.model.movefinders.KingCapturesFinder;
import com.multiplayer.draughts.utils.BoardUtils;
import com.multiplayer.draughts.utils.CaptureUtils;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

import java.util.ArrayList;
import java.util.List;

import static com.multiplayer.draughts.utils.TestFileUtils.getFullPath;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertTrue;

public class KingCapturesFinderTest {
    @Test
    public void stopsWhenLookingForKingCapture() {
        Board board = new Board();
        List<CaptureNode> captureNodes = new KingCapturesFinder(board, Color.WHITE).find();
        assertTrue(captureNodes.isEmpty());
    }

    @Test
    public void testsKingCaptures() {
        Board board = new Board(BoardUtils.readBoardFromFile(getFullPath("test_table_01.txt")));
        List<CaptureNode> captureNodes = new KingCapturesFinder(board, Color.WHITE).find();
        assertFalse(captureNodes.isEmpty());

        Assertions.assertEquals(9, CaptureUtils.findMaxCapturesCount(captureNodes.get(0)));

        List<CaptureNode> captures = new ArrayList<>();
        CaptureUtils.findDeepestPaths(captureNodes.get(0), 0, 9, captures);
        assertEquals(12, captures.size());
    }

}
