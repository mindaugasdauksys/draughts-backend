package com.multiplayer.draughts.model.movefinders;

import com.multiplayer.draughts.model.Board;
import com.multiplayer.draughts.model.Color;
import com.multiplayer.draughts.model.MoveNode;
import com.multiplayer.draughts.model.movefinders.AvailableMovesFinder;
import com.multiplayer.draughts.utils.BoardUtils;
import org.junit.jupiter.api.Test;

import java.util.List;

import static com.multiplayer.draughts.utils.TestFileUtils.getFullPath;
import static org.junit.jupiter.api.Assertions.*;

class AvailableMovesFinderTest {

    @Test
    void findsMoveWithMostCaptures() {
        Board board = new Board(BoardUtils.readBoardFromFile(getFullPath("test_table_05.txt")));
        List<MoveNode> availableMove = new AvailableMovesFinder(board, Color.WHITE).find();
        assertEquals(1, availableMove.size());
        assertEquals(new MoveNode("d4", "f6", true), availableMove.get(0));
    }

    @Test
    void testsBlacksMoves() {
        Board board = new Board(BoardUtils.readBoardFromFile(getFullPath("test_table_06.txt")));
        List<MoveNode> availableMove = new AvailableMovesFinder(board, Color.BLACK).find();
        assertEquals(6, availableMove.size());
    }

    @Test
    void testsChoiceOfLongerMove() {
        Board board = new Board(BoardUtils.readBoardFromFile(getFullPath("test_table_07.txt")));
        List<MoveNode> availableMove = new AvailableMovesFinder(board, Color.BLACK).find();
        assertEquals(1, availableMove.size());
        assertEquals(new MoveNode("b6", "d4", true), availableMove.get(0));
    }

    @Test
    void testsWhenAllBlackMovesClosed() {
        Board board = new Board(BoardUtils.readBoardFromFile(getFullPath("test_table_08.txt")));
        List<MoveNode> blackMoves = new AvailableMovesFinder(board, Color.BLACK).find();
        assertEquals(0, blackMoves.size());
    }

    @Test
    void testsWhenAllWhiteMovesClosed() {
        Board board = new Board(BoardUtils.readBoardFromFile(getFullPath("test_table_08.txt")));
        List<MoveNode> blackMoves = new AvailableMovesFinder(board, Color.WHITE).find();
        assertEquals(0, blackMoves.size());
    }

    @Test
    void testsBranchedCapture() {
        Board board = new Board(BoardUtils.readBoardFromFile(getFullPath("test_table_09.txt")));
        List<MoveNode> moves = new AvailableMovesFinder(board, Color.BLACK).find();
        assertEquals(new MoveNode("d6", "f4", true), moves.get(0));
    }

    @Test
    void capturesThroughCrowning() {
        Board board = new Board(BoardUtils.readBoardFromFile(getFullPath("test_table_10.txt")));
        List<MoveNode> moves = new AvailableMovesFinder(board, Color.BLACK).find();
        assertEquals(new MoveNode("a3", "c1", true), moves.get(0));
    }

    @Test
    void capturesOneAndHasNoMoreCapturesInCrowningPlace() {
        Board board = new Board(BoardUtils.readBoardFromFile(getFullPath("test_table_11.txt")));
        List<MoveNode> moves = new AvailableMovesFinder(board, Color.BLACK).find();
        assertEquals(new MoveNode("a3", "c1", false), moves.get(0));
    }

}