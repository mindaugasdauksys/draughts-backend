package com.multiplayer.draughts.model.movefinders;

import com.multiplayer.draughts.model.Board;
import com.multiplayer.draughts.model.Color;
import com.multiplayer.draughts.model.MoveNode;
import com.multiplayer.draughts.model.movefinders.KingMovesFinder;
import com.multiplayer.draughts.utils.BoardUtils;
import org.junit.jupiter.api.Test;

import java.util.List;

import static com.multiplayer.draughts.utils.TestFileUtils.getFullPath;
import static org.junit.jupiter.api.Assertions.*;

public class KingMovesFinderTest {

    @Test
    public void testsKingMoves() {
        Board board = new Board(BoardUtils.readBoardFromFile(getFullPath("test_table_03.txt")));
        List<MoveNode> moveNodes = new KingMovesFinder(board, Color.WHITE).find();
        assertEquals(9, moveNodes.size());
    }
}