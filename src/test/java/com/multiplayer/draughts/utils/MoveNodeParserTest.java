package com.multiplayer.draughts.utils;

import com.multiplayer.draughts.model.MoveNode;
import org.junit.jupiter.api.Test;

import java.util.List;

import static org.junit.jupiter.api.Assertions.assertEquals;

public class MoveNodeParserTest {

    @Test
    public void testsSimpleMoveParsing() {
        assertEquals(List.of(new MoveNode("a1", "b2", false),
            new MoveNode("c3", "d4", false)),
            MoveNodeParser.mapFromMoveList(List.of("a1-b2", "c3-d4")));
    }

}
