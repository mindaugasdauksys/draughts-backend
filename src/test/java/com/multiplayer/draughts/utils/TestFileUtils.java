package com.multiplayer.draughts.utils;

public class TestFileUtils {

    public static String getFullPath(String path) {
        return Thread.currentThread().getContextClassLoader().getResource(path).getPath();
    }

    private TestFileUtils() {
    }
}
