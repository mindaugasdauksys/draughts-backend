package com.multiplayer.draughts;

import com.multiplayer.draughts.model.YCoordinate;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;

public class YCoordinateTest {
    @Test
    public void testsParsingFromFullCoordinates() {
        assertEquals(YCoordinate.fromFullCoordinates("a5"), YCoordinate.N5);
    }

    @Test
    public void testsToString() {
        assertEquals(YCoordinate.N3.toString(), "3");
    }
}
